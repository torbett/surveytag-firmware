/*
 * Copyright (C) 2012 Maxim Integrated Products, Inc., All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY,  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL MAXIM INTEGRATED PRODUCTS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of Maxim Integrated Products
 * shall not be used except as stated in the Maxim Integrated Products
 * Branding Policy.
 *
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Maxim Integrated Products retains all ownership rights.
 *
 ***************************************************************************/
/** \file stm32_spi.h ******************************************************
 *
 *             Project: 
 *            Filename: stm32_spi.h
 *         Description: Header file for the spi communication example program.
 *
 *    Revision History:
 *\n                    04-05-13    Rev 1.0.0    TTS    Initial release.
 *\n                    
 *\n                    		
 *
 *  --------------------------------------------------------------------
 *
 *  This code follows the following naming conventions:
 *
 *\n    char                    	ch_pmod_value
 *\n    char (array)            s_pmod_string[16]
 *\n    float                  	 f_pmod_value
 *\n    int                    	 n_pmod_value
 *\n    int (array)             	an_pmod_value[16]
 *\n    u16                     	u_pmod_value
 *\n    u16 (array)             au_pmod_value[16]
 *\n    u8                     	 uch_pmod_value
 *\n    u8 (array)              	auch_pmod_buffer[16]
 *\n    unsigned int     	un_pmod_value
 *\n    int *                   	pun_pmod_value
 *
 *  ------------------------------------------------------------------------- */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __STM32_SPI_H
#define __STM32_SPI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"
#include "main.h"
#include "spi.h"
#include "gpio.h"
   
#define FLAG_TIMEOUT         ((uint32_t)0x1000)
#define LONG_TIMEOUT         ((uint32_t)(10 *FLAG_TIMEOUT))

// define SPI pins
#define SPI_SCK       GPIO_Pin_5    /* GPIOA.5*/
#define SPI_MISO      GPIO_Pin_6    /*GPIOA.6*/
#define SPI_MOSI      GPIO_Pin_7    /*GPIOA.7*/
#define SPI_CS        GPIO_Pin_6    /*GPIOB.6*/
#define DRDY_PIN      GPIO_Pin_5    /*GPIOB.5*/

#define SPI_GPIO_CLK             RCC_APB2Periph_GPIOA
#define SPI_CS_GPIO_CLK          RCC_APB2Periph_GPIOB
#define DRDY_GPIO_CLK            RCC_APB2Periph_GPIOB


//#define SPI_CS_HIGH              GPIO_SetBits(GPIOB, SPI_CS)
//#define SPI_CS_LOW               GPIO_ResetBits(GPIOB, SPI_CS)
   
   
#define USE_DEFAULT_CRITICAL_CALLBACK 
   
void  SPI_Inital(void);
uint8_t SPI_Write(uint8_t *buffer, uint8_t nBytes);
uint8_t SPI_Read(uint8_t *buffer, uint8_t nBytes);  
uint8_t SPI_WriteByte(uint8_t data);
uint8_t SPI_ReadByte(void);
void Set_DRDY_Pin_INPUT(void);
uint8_t DRDY_Pin_Value(void);
void Enable_Exti(void);    //enable external interrupt
void SPI_cs_Low(void);
void SPI_cs_High(void);
    
void EnterCriticalSection_UserCallback(void);
void ExitCriticalSection_UserCallback(void);   
#ifdef __cplusplus
}
#endif

#endif
