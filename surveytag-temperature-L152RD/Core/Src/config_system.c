/*
 * config_system.c
 *
 *  Created on: Jul 28, 2021
 *      Author: Dave
 */

#include <nor.h>
#include "config_system.h"
#include "serial_comms.h"
#include "ringbuffer.h"
#include "rtc.h"
#include "nor.h"
#include "lfs.h"
#include <stddef.h>

uint8_t rtc_set_flag(int8_t flag)
{
	static uint8_t flag_s = 0;
	if(flag == 1)
		flag_s = 1;
	else if(flag == -1)
		flag_s = 0;
	return flag_s;
}

static void dateTime_set(void)
{
	serial_output("Enter date/time in format DD/MM/YY HH:MM:SS (24 hour format)\r\n> ");
	const uint8_t dates_in_month[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	char *input = serial_read_line();
	if(!input)
		return;
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;

	uint8_t day,month,year;

	uint8_t options =
		sscanf(input, "%d/%d/%d %d:%d:%d",
			&day,
			&month,
			&year,
			&time.Hours,
			&time.Minutes,
			&time.Seconds);

	if( options == 6 )
	{
		if( year >= 99 )
		{
			serial_output("Invalid year\r\n");
			return;
		}
		if( month > 12 || month == 0 )
		{
			serial_output("Invalid month\r\n");
			return;
		}
		if( day > dates_in_month[month] || day == 0 )
		{
			serial_output("Invalid date\r\n");
			return;
		}
		if( time.Hours > 23 )
		{
			serial_output("Invalid hour\r\n");
			return;
		}
		if( time.Minutes > 59 )
		{
			serial_output("Invalid minute\r\n");
			return;
		}
		if( time.Seconds > 59 )
		{
			serial_output("Invalid second\r\n");
			return;
		}

		date.Date = day;
		date.Month = month;
		date.Year = year;



		if( HAL_RTC_SetDate(&hrtc, &date, RTC_FORMAT_BIN) != HAL_OK )
			serial_output("Couldn't set the date, is it valid?\r\n");
		if( HAL_RTC_SetTime(&hrtc, &time, RTC_FORMAT_BIN) != HAL_OK )
			serial_output("Couldn't set the time, is it valid?\r\n");

		serial_output("\r\nRTC set successfully!\r\n");
		rtc_set_flag(1);
	}
	else
	{
		serial_output("Invalid date time format entered\r\n");
	}
}

static void dateTime_get(void)
{
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;

	HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);

	if( rtc_set_flag(0) )
	{
		serial_output("Current date-time: \t%02d/%02d/%04d %02d:%02d:%02d\r\n",
			date.Date,
			date.Month,
			date.Year + 2000,
			time.Hours,
			time.Minutes,
			time.Seconds);
	}
	else
	{
		serial_output("Current date-time: \tNot yet set\r\n");
	}
}

static void get_current_config(void)
{
	const char tc_type_2_character[8]= {'B', 'E', 'J', 'K', 'N', 'R', 'S', 'T'};

	surveyTag_config_t *config = get_config();
	serial_output("\r\nSurveyTag " SURVEYTAG_VERSION "\r\n");
	serial_output("\r\nCurrent Configuration\r\n---------------------\r\n");
	serial_output("Logger name:\t\t%s\r\n", config->loggerName);
	serial_output("Sampling period:\t%dS\r\n", config->logInterval);
	serial_output("Burst length: \t\t%dmS\r\n", config->burstLength);
	serial_output("Thermocouple type: \t%c\r\n", tc_type_2_character[config->tc_type]);
	serial_output("On-chip samples: \t%d\r\n", config->averageSamples);
	serial_output("Calibration offset: \t%d\r\n",config->cal_offset);
	serial_output("Start type: \t\t%s\r\n", (config->startMode==startMode_Immediate)?"Immediate":"Button Press");
	dateTime_get();
}

static void sample_frequency_set(void)
{
	uint32_t log_interval;
	surveyTag_config_t* config = get_config();

	serial_output("Enter new sample period (s)\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if( sscanf(output, "%d", &log_interval) == 1)
	{
		config->logInterval = log_interval;
		write_config_sd(config);
	}
}

static void burst_length_set(void)
{
	uint32_t burst_length;
	surveyTag_config_t* config = get_config();

	serial_output("Enter new burst length (mS)\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if( sscanf(output, "%d", &burst_length) == 1)
	{
		config->burstLength = burst_length;
		write_config_sd(config);
	}
}

static void cal_offset_set(void)
{
	int16_t cal_offset;
	surveyTag_config_t* config = get_config();

	serial_output("Enter calibration offset (millidegrees)\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if( sscanf(output, "%d", &cal_offset) == 1)
	{
		config->cal_offset = cal_offset;
		write_config_sd(config);
	}
}

static void log_start_set(void)
{
	surveyTag_config_t* config = get_config();

	serial_output("Select log start mode: I=immediate, B=button press\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if ((output[0] == 'i') || (output[0] == 'I'))
	{
		config->startMode = startMode_Immediate;
		write_config_sd(config);
	} else if ((output[0] == 'b') || (output[0] == 'B'))
	{
		config->startMode = startMode_ButtonPress;
		write_config_sd(config);
	}
	else serial_output("\r\nInvalid entry\r\n");

}


static void confirm_format(void)
{
	serial_output("Format all data: Are you sure? Y/N\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if ((output[0] == 'y') || (output[0] == 'Y'))
	{
		serial_output("\r\nFomatting flash... ");
		do_format();
		store_default_config();
		read_config_sd(get_config());
		serial_output("Done!\r\n");
	}

}



static void thermocouple_type_set(void)
{
	thermocoupleType_t thermocouple = 0xFF;
	surveyTag_config_t* config = get_config();

	serial_output("Enter new thermocouple type (B/E/J/K/N/R/S/T)\r\n> ");

	while( usb_connected() )
	{
		char rx_byte = serial_get_char(1);

		switch(rx_byte)
		{
			case 'b':
			case 'B':
				thermocouple = tc_typeB;
				break;
			case 'e':
			case 'E':
				thermocouple = tc_typeE;
				break;
			case 'j':
			case 'J':
				thermocouple = tc_typeJ;
				break;
			case 'k':
			case 'K':
				thermocouple = tc_typeK;
				break;
			case 'n':
			case 'N':
				thermocouple = tc_typeN;
				break;
			case 'r':
			case 'R':
				thermocouple = tc_typeR;
				break;
			case 's':
			case 'S':
				thermocouple = tc_typeS;
				break;
			case 't':
			case 'T':
				thermocouple = tc_typeT;
				break;
			case 0x1B: //Escape key
				thermocouple = 0xFF;
		}

		if( thermocouple != 0xFF )
		{
			serial_output("%c\r\n", rx_byte);
			config->tc_type = thermocouple;
			write_config_sd(config);
			break;
		}
	}
}

static void sample_average_set(void)
{
	uint32_t sample_average;
	surveyTag_config_t* config = get_config();

	serial_output("Enter number of on-device samples to take (1/2/4/8/16)\r\n> ");

	const char* output = serial_read_line();
	if( !output )
		return;

	if( sscanf(output, "%d", &sample_average) == 1)
	{
		if( sample_average == 1 || sample_average == 2 || sample_average == 4 || sample_average == 8 || sample_average == 16 )
		{
			config->averageSamples = sample_average;
			write_config_sd(config);
		}
		else
		{
			serial_output("\r\nSample average must be 1, 2, 4, 8 or 16\r\n");
		}
	}
	else
	{
		serial_output("\r\nNumber couldn't be parsed\r\n");
	}
}

static void delete_readings(void)
{
	serial_output("Do you wish to delete the readings? Y/N\r\n> ");
	char* confirm = serial_read_line();
	if( !confirm )
		return;
	if( confirm[0] == 'y' || confirm[0] == 'Y' )
	{
		lfs_init();
		lfs_delete(DATA_FILE_PATH);
		lfs_deinit();
	}
}

static void logger_name_set(void)
{
	surveyTag_config_t* config = get_config();

	serial_output("Enter new logger name\r\n> ");

	const char* name = serial_read_line();
	if( !name )
		return;

	if( strlen(name) < 30 )
	{
		strcpy(config->loggerName, name);
		write_config_sd(config);
	}
	else
	{
		serial_output("\r\nName was too long\r\n");
	}
}


void print_config_options(void)
{
	static const char* options[] =
	{
		"Get Current Config",
		"Set Date & Time",
        "Set Sample Period",
		"Set Sample Burst Length",
		"Set Log Start Mode",
		"Set Thermocouple Type",
		"Set On-chip sample average count",
		"Set Logger Name",
		"Set Display Contrast",
		"Set Calibration Offset",
		"Write Out Readings",
		"Delete Readings",
		"Format Flash WARNING: will erase all settings and data!",
		"Go into sleep mode",
		NULL
	};

	switch( serial_print_menu(options) )
	{
		case 0:
			get_current_config();
			break;
		case 1:
			dateTime_set();
			break;
		case 2:
			sample_frequency_set();
			break;
		case 3:
			burst_length_set();
			break;
		case 4:
			log_start_set();
			break;
		case 5:
			thermocouple_type_set();
			break;
		case 6:
			sample_average_set();
			break;
		case 7:
			logger_name_set();
			break;
		case 8:
			serial_output("\r\nNot currently implemented\r\n");
			break;
		case 9:
			cal_offset_set();
			break;
		case 10:
			write_out_data();
			delete_readings();
			break;
		case 11:
			delete_readings();
			break;
		case 12:
			confirm_format();
			break;
		case 13:
			rtc_set_flag(-1);
			serial_output("\r\nPlease disconnect the USB and the device will return into it's sleep state\r\n");
			break;
	}
}
