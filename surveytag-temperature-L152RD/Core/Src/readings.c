/*
 * readings.c
 *
 *  Created on: Jul 23, 2021
 *      Author: Dave
 */


#include "readings.h"
#include "power.h"
#include "lcd_user.h"

#include <math.h>
#include <nor.h>

volatile uint32_t end_tics;
volatile surveyTag_reading_t current_read_burst;
volatile uint8_t m_currently_sampling = 0;

static void record_sample_output(void)
{
	current_read_burst.tc_reading /= current_read_burst.num_readings;
	current_read_burst.cold_junction_reading /= current_read_burst.num_readings;

	char char1, char2, char3;
	bool leadingOne = false;
	bool leadingNegative = false;

	char1 = (uint8_t)floor((int)(abs(current_read_burst.tc_reading)/10) % 10) + 48;
	char2 = (uint8_t)floor((int)(abs(current_read_burst.tc_reading))% 10) + 48;
	char3 = (uint8_t)floor(abs((int)((current_read_burst.tc_reading)*10)% 10)) + 48;
	if (abs(current_read_burst.tc_reading) > 100) leadingOne = true;
	if (current_read_burst.tc_reading < 0 ) leadingNegative = true;

	updateDisplay(char1, char2, char3, current_read_burst.batteryLevel, leadingOne, leadingNegative, true, true, false, 0);
}

static void store_sample_metadata(void)
{
	RTC_TimeTypeDef currentTime;
	RTC_DateTypeDef currentDate;

	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);

	current_read_burst.second = currentTime.Seconds;
	current_read_burst.minute = currentTime.Minutes;
	current_read_burst.hour= currentTime.Hours;
	current_read_burst.day = currentDate.Date;
	current_read_burst.month = currentDate.Month;
	current_read_burst.year = currentDate.Year + 2000;

	current_read_burst.cold_junction_reading = 0;
	current_read_burst.tc_reading = 0;
	current_read_burst.num_readings = 0;

	current_read_burst.batteryLevel = readBatteryLevel();


}

surveyTag_Result start_temp_sampling(surveyTag_config_t *config)
{
	uint8_t average_samples;

	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);

	HAL_SPI_Init(&hspi2);

	HAL_GPIO_WritePin(THERM_PWR_EN_GPIO_Port, THERM_PWR_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(1);

	switch( config->averageSamples )
	{
		case 1:
			average_samples = 0;
			break;
		case 2:
			average_samples = Average_2;
			break;
		case 4:
			average_samples = Average_4;
			break;
		case 8:
			average_samples = Average_8;
			break;
		case 16:
			average_samples = Average_16;
			break;
		default:
			return st_ERROR;
	}

	maxim_31856_init(config->tc_type,  average_samples);
	maxim_set_oc_fault_detection(0);

	m_currently_sampling = 1;

	HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b10000000, 0b10000000);
	HAL_LCD_UpdateDisplayRequest(&hlcd);

	current_read_burst.cold_junction_reading = 0;
	current_read_burst.tc_reading = 0;
	current_read_burst.num_readings = 0;

	store_sample_metadata();
	end_tics = HAL_GetTick() + config->burstLength;
	maxim_clear_fault_status();
	maxim_start_conversion(Automatic_Conversion);
	//HAL_SuspendTick();
	//__HAL_RCC_PWR_CLK_ENABLE();
	//HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
	//HAL_ResumeTick();
	//configure_clocks();
}


surveyTag_Result temp_sample_cb(surveyTag_config_t *config)
{
	uint32_t temperature_value = 0;
	uint8_t uch_reg[16] = {0};
	uint8_t uch_cjth,uch_cjtl,uch_ltcbh,uch_ltcbm,uch_ltcbl,uch_sr;

	HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b10000000, 0b10000000);
	HAL_LCD_UpdateDisplayRequest(&hlcd);

	maxim_31856_read_nregisters(0x0A, uch_reg, 6);

	uch_cjth=uch_reg[0x00];
	uch_cjtl=uch_reg[0x01];
	uch_ltcbh=uch_reg[0x02];
	uch_ltcbm=uch_reg[0x03];
	uch_ltcbl=uch_reg[0x04];
	uch_sr=uch_reg[0x05];

	if( uch_sr == NO_Fault && current_read_burst.fault_code == NO_Fault )
	{
		//	current_read_burst.num_readings++;

		temperature_value = (( (uint32_t)uch_cjth << 8 ) + ( (uint32_t) uch_cjtl) ) >> 2;
		if( (uch_cjth & 0x80) == 0x80 )
		{
			temperature_value = 0x3FFF - temperature_value + 1;
			current_read_burst.cold_junction_reading += 0 - temperature_value * Cold_Junction_Resolution;
		}
		else
		{
			current_read_burst.cold_junction_reading += temperature_value * Cold_Junction_Resolution;
		}

		temperature_value = (((uint32_t)uch_ltcbh<<16) + ((uint32_t) uch_ltcbm << 8) + (uint32_t) uch_ltcbl) >> 5;
		if( (uch_ltcbh & 0x80) == 0x80 )
		{
			temperature_value = 0x7FFFF - temperature_value + 1;
			current_read_burst.tc_reading += ((0 - temperature_value * TC_Resolution)+((float)config->cal_offset/1000));
		}
		else
		{
			current_read_burst.tc_reading += ((temperature_value * TC_Resolution)+((float)config->cal_offset/1000));
		}
		current_read_burst.num_readings++;
	}
	else
	{
		current_read_burst.fault_code = uch_sr;
	}

	if( HAL_GetTick() > end_tics )
	{
		maxim_stop_conversion();
		m_currently_sampling = 0;
	}
}


surveyTag_Result end_temp_sampling(surveyTag_reading_t **reading)
{
	HAL_GPIO_WritePin(THERM_PWR_EN_GPIO_Port, THERM_PWR_EN_Pin, GPIO_PIN_SET);
	HAL_SPI_DeInit(&hspi2);
	//HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
	record_sample_output();
	m_currently_sampling = 0;
	if(reading != NULL)
		*reading = &current_read_burst;
}

uint8_t currently_temp_sampling(void)
{
	return m_currently_sampling;
}
