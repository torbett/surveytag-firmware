/*
 * sd.c
 *
 *  Created on: 26 Jul 2021
 *      Author: Dave
 */
#include "nor.h"
#include "serial_comms.h"
#include "lfs.h"
#include "usbd_cdc_if.h"
#include "surveyTag.h"

const surveyTag_config_t default_config = {
	.loggerName = "SurveyTag Logger",
	.logInterval = 10,
	.averageSamples = 16,
	.tc_type = tc_typeK,
	.contrast = 4,
	.burstLength = 0,
	.cal_offset = 0,
	.startMode = startMode_Immediate,
};

surveyTag_Result read_config_sd(surveyTag_config_t *config)
{
	int result;
	lfs_file_t config_file;

	if ( lfs_init() != st_OK )
		return st_ERROR;

	result = lfs_open(&config_file, CONFIG_FILE_PATH, LFS_O_RDONLY);
	if( result != st_OK )
		return st_ERROR;

	result = lfs_read(&config_file, config, sizeof(*config));
	if( result != st_OK )
			return st_ERROR;

	result = lfs_close(&config_file);
	if( result != st_OK )
			return st_ERROR;

	lfs_deinit();
	return st_OK;
}

surveyTag_Result write_config_sd(const surveyTag_config_t *config)
{
	int result;
	lfs_file_t config_file;

	if ( lfs_init() != st_OK )
		return st_ERROR;

	result = lfs_open(&config_file, CONFIG_FILE_PATH, LFS_O_WRONLY | LFS_O_CREAT | LFS_O_TRUNC);
	if( result != st_OK )
		return st_ERROR;

	result = lfs_write(&config_file, config, sizeof(*config));
	if( result != st_OK )
		return st_ERROR;

	result = lfs_close(&config_file);
	if( result != st_OK )
			return st_ERROR;

	lfs_deinit();
	return st_OK;
}

surveyTag_Result store_reading_sd(surveyTag_reading_t *reading)
{
	int result;
	lfs_file_t reading_file;

	HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b10000000, 0b10000000);
	HAL_LCD_UpdateDisplayRequest(&hlcd);

	if ( lfs_init() != st_OK )
		return st_ERROR;

	result = lfs_open(&reading_file, DATA_FILE_PATH, LFS_O_WRONLY | LFS_O_APPEND | LFS_O_CREAT );
	if( result != st_OK )
		return st_ERROR;

	result = lfs_write(&reading_file, reading, sizeof(*reading));
	if( result != st_OK )
			return st_ERROR;

	result = lfs_close(&reading_file);
	if( result != st_OK )
			return st_ERROR;

	lfs_deinit();

	HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b10000000, 0b10000000);
	HAL_LCD_UpdateDisplayRequest(&hlcd);

	return st_OK;
}

const char* CSVheader= "Year,\tMonth,\tDay,\tHour,\tMinute,\tSecond,\tTC_Reading,\tCJ_Reading,\tvBatt,\tSamples,Fault_Code\r\n";

surveyTag_Result write_out_data(void)
{
	int result;
	lfs_file_t reading_file;
	surveyTag_reading_t current_reading;
	serial_output("-----\r\n");

	if ( lfs_init() != st_OK )
		return st_ERROR;

	result = lfs_open(&reading_file, DATA_FILE_PATH, LFS_O_RDONLY);
	if( result != st_OK )
		return st_ERROR;

	size_t file_size;
	result = lfs_size(&reading_file, &file_size);
	if( result != st_OK )
		return st_ERROR;

	if( file_size == 0 )
		goto close;

	uint32_t n_records = file_size / sizeof(surveyTag_reading_t);

	CDC_Transmit_FS(CSVheader, strlen(CSVheader));

	for(uint32_t i = 0; i < n_records; i++)
	{
		result = lfs_read(&reading_file, &current_reading, sizeof(current_reading));
		if( result != st_OK )
			return st_ERROR;

		serial_output("%04d,\t%02d,\t%02d,\t%02d,\t%02d,\t%02d,\t%.5f,\t%.5f,\t%04d,\t%d,\t%d\r\n",
				current_reading.year,
				current_reading.month,
				current_reading.day,
				current_reading.hour,
				current_reading.minute,
				current_reading.second,
				current_reading.tc_reading,
				current_reading.cold_junction_reading,
				current_reading.batteryLevel,
				current_reading.num_readings,
				current_reading.fault_code);
	}
close:
	result = lfs_close(&reading_file);
	if( result != st_OK )
			return st_ERROR;

	lfs_deinit();
}

void store_default_config()
{

	const uint16_t* UNIQUE_A = 0x1FF800D0;
	const uint16_t* UNIQUE_B = 0x1FF800D0 + 0x02;
	const uint16_t* UNIQUE_C = 0x1FF800D0 + 0x04;
	const uint16_t* UNIQUE_D = 0x1FF800D0 + 0x06;
	const uint16_t* UNIQUE_E = 0x1FF800D0 + 0x14;
	const uint16_t* UNIQUE_F = 0x1FF800D0 + 0x16;

	uint16_t id = *UNIQUE_A ^ *UNIQUE_B ^ *UNIQUE_C ^ *UNIQUE_D ^ *UNIQUE_E ^ *UNIQUE_F;

	surveyTag_config_t config_to_write;
	memcpy(&config_to_write, &default_config, sizeof(surveyTag_config_t));
	snprintf(config_to_write.loggerName, 50, "SurveyTag-%04X", id);

	write_config_sd(&config_to_write);
}
