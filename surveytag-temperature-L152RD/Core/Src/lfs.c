/*
 * lfs.c
 *
 *  Created on: Jul 29, 2021
 *      Author: Dave
 */

#include "lfs.h"
#include "flash.h"
#include "serial_comms.h"
#include "surveyTag.h"
#include "../../Middlewares/Third_Party/flash_driver/src/spiflash.h"

lfs_t lfs;
uint8_t lfs_opened = 0;
extern spiflash_t spif;

static int read(const struct lfs_config *c, lfs_block_t block,
        lfs_off_t off, void *buffer, lfs_size_t size)
{
	uint32_t address = block * FLASH_BLOCK_SZE + off;

	if( address % FLASH_PAGE_SZE != 0 )
		return LFS_ERR_IO;

	int result = SPIFLASH_read(&spif, address, size, buffer);

	return (result == SPIFLASH_OK) ? LFS_ERR_OK : LFS_ERR_IO;
}

static int prog(const struct lfs_config *c, lfs_block_t block,
        lfs_off_t off, const void *buffer, lfs_size_t size)
{
	uint32_t address = block * FLASH_BLOCK_SZE + off;

	if( address % FLASH_PAGE_SZE != 0 )
		return LFS_ERR_IO;

	int result = SPIFLASH_write(&spif, address, size, buffer);

	return (result == SPIFLASH_OK) ? LFS_ERR_OK : LFS_ERR_IO;
}

static int erase(const struct lfs_config *c, lfs_block_t block)
{
	uint32_t address = block * FLASH_BLOCK_SZE;
	int result = SPIFLASH_erase(&spif, address, FLASH_BLOCK_SZE);

	return (result == SPIFLASH_OK) ? LFS_ERR_OK : LFS_ERR_IO;
}



static int sync(const struct lfs_config *c)
{
	return LFS_ERR_OK;
}


// configuration of the filesystem is provided by this struct
const struct lfs_config cfg = {
    // block device operations
    .read  = read,
    .prog  = prog,
    .erase = erase,
    .sync  = sync,

    // block device configuration
    .read_size = FLASH_PAGE_SZE,
    .prog_size = FLASH_PAGE_SZE,
    .block_size = FLASH_BLOCK_SZE,
    .block_count = FLASH_BLOCK_CNT,
    .cache_size = FLASH_BLOCK_SZE,
    .lookahead_size = 128,
    .block_cycles = 200,
};

surveyTag_Result lfs_open(lfs_file_t *file, const char* path, int flags)
{
	int lfs_result = lfs_file_open(&lfs, file, path, flags);

	return (lfs_result == LFS_ERR_OK) ? st_OK : st_ERROR;
}

surveyTag_Result lfs_read(lfs_file_t *file, void* data, size_t len)
{
	int lfs_result = lfs_file_read(&lfs, file, data, len);

	return (lfs_result >= 0) ? st_OK : st_ERROR;
}

surveyTag_Result lfs_write(lfs_file_t *file, void* data, size_t len)
{
	int lfs_result = lfs_file_write(&lfs, file, data, len);

	return (lfs_result >= 0) ? st_OK : st_ERROR;
}

surveyTag_Result lfs_size(lfs_file_t *file, size_t *size)
{
	int result = lfs_file_size(&lfs, file);
	if (result < 0)
		return st_ERROR;
	*size = result;
	return st_OK;
}

surveyTag_Result lfs_close(lfs_file_t *file)
{
	int lfs_result = lfs_file_close(&lfs, file);

	return (lfs_result == LFS_ERR_OK) ? st_OK : st_ERROR;
}

surveyTag_Result lfs_delete(const char* path)
{
	int lfs_result = lfs_remove(&lfs, path);

	return (lfs_result == LFS_ERR_OK) ? st_OK : st_ERROR;
}

surveyTag_Result do_format(void)
{
	int result;

	/*result = lfs_unmount(&lfs);
	if( result != LFS_ERR_OK )
		serial_output("Error unmounting: %d\r\n", result);*/

	spi_flash_init();

	result = lfs_format(&lfs, &cfg);
	if( result != LFS_ERR_OK )
	{
		serial_output("Error formatting: %d\r\n", result);
		goto fail;
	}

	result = lfs_mount(&lfs, &cfg);
	if( result != LFS_ERR_OK )
	{
		serial_output("Error mounting: %d\r\n", result);
		goto fail;
	}
fail:
	lfs_deinit();
	return result;
}

surveyTag_Result lfs_init(void)
{
	int result;

	if(lfs_opened)
	{
		serial_output("LFS already opened\r\n");
		return st_OK;
	}
	lfs_opened = 1;

	spi_flash_init();

	result = lfs_mount(&lfs, &cfg);
	if( result != LFS_ERR_OK )
	{
		result = lfs_format(&lfs, &cfg);
		if( result != LFS_ERR_OK )
			return st_ERROR;

		result = lfs_mount(&lfs, &cfg);
		if( result != LFS_ERR_OK )
			return st_ERROR;
	}
	return st_OK;
}

surveyTag_Result lfs_deinit(void)
{
	lfs_unmount(&lfs);
	spi_flash_deinit();
	lfs_opened = 0;
	return st_OK;
}
