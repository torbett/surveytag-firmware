/*
 * flash.c
 *
 *  Created on: Jul 29, 2021
 *      Author: Dave
 */
#include "spi.h"
#include "gpio.h"
#include "flash.h"
#include "../../Middlewares/Third_Party/flash_driver/src/spiflash.h"

spiflash_t spif;

int impl_spiflash_spi_txrx(spiflash_t *spi, const uint8_t *tx_data, uint32_t tx_len, uint8_t *rx_data, uint32_t rx_len)
{
	int res = SPIFLASH_OK;
	if (tx_len > 0) {
	// first transmit tx_len bytes from tx_data if needed
		res = (HAL_SPI_Transmit(&hspi3, tx_data, tx_len, 10000) == HAL_OK) ? SPIFLASH_OK : SPIFLASH_ERR_HW_BUSY;
	}

	if (res == SPIFLASH_OK && rx_len > 0) {
	// then receive rx_len bytes into rx_data if needed
		res = (HAL_SPI_Receive(&hspi3, rx_data, rx_len, 10000) == HAL_OK) ? SPIFLASH_OK : SPIFLASH_ERR_HW_BUSY;
	}

	return res;
}

void impl_spiflash_spi_cs(spiflash_t *spi, uint8_t cs)
{
	if (cs)
	{
		// assert cs pin
		HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, RESET);
	}
	else
	{
		// de assert cs pin
		HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, SET);
	}
}

void impl_spiflash_wait(spiflash_t *spi, uint32_t ms)
{
	HAL_Delay(ms);
}

const spiflash_cmd_tbl_t my_spiflash_cmds = SPIFLASH_CMD_TBL_STANDARD;

const spiflash_config_t my_spiflash_config = {
  .sz = FLASH_SZE,
  .page_sz = FLASH_PAGE_SZE, // normally 256 byte pages
  .addr_sz = 3, // normally 3 byte addressing
  .addr_dummy_sz = 0, // using single line data, not quad or something
  .addr_endian = SPIFLASH_ENDIANNESS_BIG, // normally big endianess on addressing
  .sr_write_ms = 40,
  .page_program_ms = 2,
  .block_erase_4_ms = 400,
  .block_erase_8_ms = 0, // not supported
  .block_erase_16_ms = 0, // not supported
  .block_erase_32_ms = 2000,
  .block_erase_64_ms = 3000,
  .chip_erase_ms = 210000
};


static const spiflash_hal_t my_spiflash_hal = {
		._spiflash_spi_txrx = impl_spiflash_spi_txrx,
		._spiflash_spi_cs = impl_spiflash_spi_cs,
		._spiflash_wait = impl_spiflash_wait
};

void spi_flash_init(void)
{
	MX_SPI3_Init();
	HAL_GPIO_WritePin(FLASH_PWR_EN_GPIO_Port, FLASH_PWR_EN_Pin, RESET);
	HAL_Delay(100);

	SPIFLASH_init(&spif,
	                &my_spiflash_config,
	                &my_spiflash_cmds,
	                &my_spiflash_hal,
	                0,
	                SPIFLASH_SYNCHRONOUS,
	                NULL);

	//Enable 4 byte addressing on flash
	/*const uint8_t* EN_4BYTE_MODE = 0xB7; //From flash DS
	impl_spiflash_spi_cs(&spif, 1);
	impl_spiflash_spi_txrx(&spif, EN_4BYTE_MODE, 1, NULL, 0);
	impl_spiflash_spi_cs(&spif, 0);*/
}

void spi_flash_deinit(void)
{
	HAL_GPIO_WritePin(FLASH_PWR_EN_GPIO_Port, FLASH_PWR_EN_Pin, SET);
}
