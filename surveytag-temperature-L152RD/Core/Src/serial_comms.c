/*
 * serial_menu.c
 *
 *  Created on: 23 Jul 2021
 *      Author: Dave
 */

#include "serial_comms.h"
#include "usbd_cdc_if.h"
#include "ringbuffer.h"
#include "surveyTag.h"
#include <stdarg.h>
#include <string.h>

char serial_buffer[160];

static const char* newline = "\r\n";

void serial_output(const char* format, ...)
{
    va_list args;

    va_start(args, format);
    vsprintf(serial_buffer, format, args);
    va_end(args);
    CDC_Transmit_FS(serial_buffer, strlen(serial_buffer));
}

char* serial_read_line(void)
{
	static char input_string[50];
	char *input_i = input_string;
	const char delete[3] = {0x08, ' ', 0x08};

	while( usb_connected() && (input_i - input_string < 50) )
	{
		*input_i = serial_get_char(0);

		if( *input_i == 0x1B ) //Escape key
			return NULL;

		if( *input_i == 0x7F || *input_i == 0x08 ) //Backspace character
		{
			if( input_i > input_string )
			{
				*input_i = 0x00;
				input_i--;
				CDC_Transmit_FS(delete, 3);
			}
			continue;
		}

		CDC_Transmit_FS(input_i, 1);

		if( (*input_i == '\n') || (*input_i == '\r') )
		{
			*input_i = 0;
			break;
		}
		input_i++;
	}
	CDC_Transmit_FS("\r\n", 2);
	RingBufferU8_clear(get_usb_ringbuffer()); //Clear any bytes in buffer in case something sent \n
	if( !usb_connected() )
		return NULL;
	return input_string;
}

char serial_get_char(uint8_t flush_buffer)
{
	if( flush_buffer )
		RingBufferU8_clear(get_usb_ringbuffer());

	while( usb_connected() )
	{
		if( RingBufferU8_available(get_usb_ringbuffer()) )
			break;
	}
	return RingBufferU8_readByte(get_usb_ringbuffer());
}

uint8_t serial_print_menu(const char* options)
{
	static const char* bracket = ") ";
	static const char* enter_sel = ">";
	uint8_t option_count = 0;

	char byte = 0xFF;
	const char* const* option_i = options;
	char menu_character = 'A';

	RingBufferU8_clear(get_usb_ringbuffer());

	CDC_Transmit_FS(newline, 2);
	while(*option_i)
	{
		const char* option = *option_i++;
		size_t len = strlen(option);

		CDC_Transmit_FS(&menu_character, 1);
		CDC_Transmit_FS(bracket, 2);
		CDC_Transmit_FS(option, len);
		CDC_Transmit_FS(newline, 2);

		menu_character++;
		option_count++;
	}
	CDC_Transmit_FS(newline, 2);
	CDC_Transmit_FS(enter_sel, strlen(enter_sel));

	while( usb_connected() )
	{
		char* line = serial_read_line();
		byte = line[0];
		if( byte == 0 )
			byte = 'A';
		if ( (byte >= 'a') && (byte <= 'z') )
			byte -= 32;

		byte -= 'A';

		if ( byte < option_count )
			break;
	}

	return byte;
}


