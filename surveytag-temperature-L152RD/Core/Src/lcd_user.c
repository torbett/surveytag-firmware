/*
 * lcd_user.c
 *
 *  Created on: 26 Jul 2021
 *      Author: Dave
 */

#include <lcd_user.h>

surveyTag_Result initDisplay(void)
{
	HAL_LCD_Clear(&hlcd);
	HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, 0xFFFF, 0xFFFF);
	HAL_LCD_UpdateDisplayRequest(&hlcd);

	return st_OK;
}

surveyTag_Result updateDisplay(uint8_t char1, uint8_t char2, uint8_t char3, uint16_t batteryLevel, bool leadingOne, bool leadingNegative, bool degC, bool decPoint, bool percent, uint8_t signal_level)
{
	// start by clearing the RAM
	  HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, 0x0, 0);
	  HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, 0x0, 0);
	  HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, 0x0, 0);
	  HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, 0x0, 0);


	if (leadingNegative) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b100, 0b100);
	if (leadingOne) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b1, 0b1);
	if (degC) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b1000000, 0b1000000);
	if (percent) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b1000000, 0b1000000);
	if (decPoint) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b10000, 0b10000);

	// todo: battery level

	// -[   ]  <= 1.1v
	// -[  #] 1.1 < v <= 1.23
	// -[ ##] 1.23 < v <= 1.366
	// -[###] > 1.366


	if (batteryLevel > 500) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b100000000, 0b100000000);
	if (batteryLevel > 900) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b100000000, 0b100000000);
	if (batteryLevel > 1100) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b100000000, 0b100000000);
	if (batteryLevel > 1300) HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b100000000, 0b100000000);




	// characters that we can do
	// 0 1 2 3 4 5 6 7 8 9 -
	// AbcdEFGhiJLnoPqrStUy
	//


	// Seven segment display mappings
	//
	//
	//        ____A_____
	//       |          |
	//    F  |          | B
	//       |          |
	//       |____G_____|
	//       |          |
	//    E  |          | C
	//       |          |
	//       |__________|
	//            D
	//
	//
	//                         S8 S7 S6 S5 S4 S3 S2 S1 S0
	// LCD_RAM_REGISTER0 COM0  B1 S1  x 3D  P  2D - 1D  1
	// LCD_RAM_REGISTER2 COM1  B2 S2  x 3C 3E 2C 2E 1C 1E
	// LCD_RAM_REGISTER4 COM2  B3 S3  % 3B 3G 2B 2G 1B 1G
	// LCD_RAM_REGISTER6 COM3  B4 S4 °C 3A 3F 2A 2F 1A 1F
	//
	//  B1-4 = battery S1-4 = signal P = decimal place 1 = leading 1 - = leading -ve sign

	// 0 = ABCDEF
	// 1 = BC
	// 2 = ABDEG
	// 3 = ABCDG
	// 4 = BCFG
	// 5 = ACDFG
	// 6 = ACDEFG
	// 7 = ACB
	// 8 = ABCDEFG
	// 9 = ABCDFG


	// b = CDEFG
	// c = DEG
	// C = ABEFG
	// d = BCDEG
	// E = ADEFG
	// F = AEFG
	// g = ABCDFG
	// G = ACDEF
	// h = CEFG
	// H = BCEFG
	// I = BC
	// J = BCDE
	// L = DEF
	// o = CDEG
	// O = ABCDEF
	// P = ABEFG
	// q = ABCFG
	// r = EG
	// S = ACDFG
	// t = DEFG
	// U = BCDEF
	// u = CDE
	// y = BCDFG

	if (char1=='0')
	{
		LCD_1A;
		LCD_1B;
		LCD_1C;
		LCD_1D;
		LCD_1E;
		LCD_1F;

	}
	if (char2=='0')
	{
		LCD_2A;
		LCD_2B;
		LCD_2C;
		LCD_2D;
		LCD_2E;
		LCD_2F;

	}
	if (char3=='0')
	{
		LCD_3A;
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3E;
		LCD_3F;

	}
	if (char1=='1')
	{

		LCD_1B;
		LCD_1C;


	}
	if (char2=='1')
	{

		LCD_2B;
		LCD_2C;


	}
	if (char3=='1')
	{

		LCD_3B;
		LCD_3C;
	}
	if (char1=='2')
	{
		LCD_1A;
		LCD_1B;
		LCD_1D;
		LCD_1E;
		LCD_1G;

	}
	if (char2=='2')
	{

		LCD_2A;
		LCD_2B;
		LCD_2D;
		LCD_2E;
		LCD_2G;


	}
	if (char3=='2')
	{

		LCD_3A;
		LCD_3B;
		LCD_3D;
		LCD_3E;
		LCD_3G;

	}

	if (char1=='3')
	{
		LCD_1A;
		LCD_1B;
		LCD_1C;
		LCD_1D;
		LCD_1G;

	}
	if (char2=='3')
	{

		LCD_2A;
		LCD_2B;
		LCD_2C;
		LCD_2D;
		LCD_2G;


	}
	if (char3=='3')
	{

		LCD_3A;
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3G;

	}
	if (char1=='4')
	{
		LCD_1B;
		LCD_1C;
		LCD_1F;
		LCD_1G;

	}
	if (char2=='4')
	{

		LCD_2B;
		LCD_2C;
		LCD_2F;
		LCD_2G;


	}
	if (char3=='4')
	{

		LCD_3B;
		LCD_3C;
		LCD_3F;
		LCD_3G;

	}
	if ((char1=='5') || (char1=="S"))
	{
		LCD_1A;
		LCD_1C;
		LCD_1D;
		LCD_1F;
		LCD_1G;

	}
	if (char2=='5')
	{

		LCD_2A;
		LCD_2C;
		LCD_2D;
		LCD_2F;
		LCD_2G;


	}
	if (char3=='5')
	{
		LCD_3A;
		LCD_3C;
		LCD_3D;
		LCD_3F;
		LCD_3G;

	}

	if (char1=='6')
	{
		LCD_1A;
		LCD_1C;
		LCD_1D;
		LCD_1E;
		LCD_1F;
		LCD_1G;

	}
	if (char2=='6')
	{

		LCD_2A;
		LCD_2C;
		LCD_2D;
		LCD_2E;
		LCD_2F;
		LCD_2G;


	}
	if (char3=='6')
	{
		LCD_3A;
		LCD_3C;
		LCD_3D;
		LCD_3E;
		LCD_3F;
		LCD_3G;

	}
	if (char1=='7')
	{
		LCD_1A;
		LCD_1B;
		LCD_1C;
	}
	if (char2=='7')
	{

		LCD_2A;
		LCD_2B;
		LCD_2C;
	}
	if (char3=='7')
	{
		LCD_3A;
		LCD_3B;
		LCD_3C;
	}
	if (char1=='8')
	{
		LCD_1A;
		LCD_1B;
		LCD_1C;
		LCD_1D;
		LCD_1E;
		LCD_1F;
		LCD_1G;

	}
	if (char2=='8')
	{

		LCD_2A;
		LCD_2B;
		LCD_2C;
		LCD_2D;
		LCD_2E;
		LCD_2F;
		LCD_2G;
	}
	if (char3=='8')
	{
		LCD_3A;
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3E;
		LCD_3F;
		LCD_3G;;
	}
	if (char1=='9')
	{
		LCD_1A;
		LCD_1B;
		LCD_1C;
		LCD_1D;
		LCD_1F;
		LCD_1G;

	}
	if (char2=='9')
	{

		LCD_2A;
		LCD_2B;
		LCD_2C;
		LCD_2D;
		LCD_2F;
		LCD_2G;
	}
	if (char3=='9')
	{
		LCD_3A;
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3F;
		LCD_3G;;
	}
	if (char1=='L')
	{
		LCD_1D;
		LCD_1E;
		LCD_1F;
	}
	if (char2=='o')
	{
		LCD_2C;
		LCD_2D;
		LCD_2E;
		LCD_2G;
	}
	if (char3=='g')
	{
		LCD_3A;
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3F;
		LCD_3G;
	}
	if (char2=='E')
	{
		LCD_2A;
		LCD_2D;
		LCD_2E;
		LCD_2F;
		LCD_2G;
	}
	if (char3=='t')
	{
		LCD_3D;
		LCD_3E;
		LCD_3F;
		LCD_3G;
	}
	if (char1=='r')
	{
		LCD_1E;
		LCD_1G;
	}
	if (char2=='d')
	{
		LCD_2B;
		LCD_2C;
		LCD_2D;
		LCD_2E;
		LCD_2G;
	}
	if (char3=='y')
	{
		LCD_3B;
		LCD_3C;
		LCD_3D;
		LCD_3F;
		LCD_3G;
	}
	if (char1=='U')
	{
		LCD_1B;
		LCD_1C;
		LCD_1D;
		LCD_1E;
		LCD_1F;
	}
	if (char3=='b')
	{
		LCD_3C;
		LCD_3D;
		LCD_3E;
		LCD_3F;
		LCD_3G;
	}
	if (char1=='-')
	{
		LCD_1G;
	}
	if (char2=='-')
	{
		LCD_2G;
	}
	if (char3=='-')
	{
		LCD_3G;
	}

	HAL_LCD_UpdateDisplayRequest(&hlcd);

	return st_OK;
}
