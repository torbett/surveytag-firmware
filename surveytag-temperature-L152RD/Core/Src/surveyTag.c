/*
//
//  _____ ___  ___ ___ ___ _____ _____
// |_   _/ _ \| _ \ _ ) __|_   _|_   _|
//   | || (_) |   / _ \ _|  | |   | |
//  _|_| \___/|_|_\___/___| |_|   |_|
// |   \ ___ __(_)__ _ _ _
// | |) / -_|_-< / _` | ' \
// |___/\___/__/_\__, |_||_|
//               |___/
//
// SurveyTag
// (c) Torbett Design Ltd 2021
 *
 * surveyTag.c
 * Main SurveyTag functions
//
*/
#include "surveyTag.h"
#include "lcd_user.h"
#include "serial_comms.h"
#include "power.h"
#include "readings.h"
#include "lfs.h"
#include <math.h>
#include "nor.h"
#include "config_system.h"

uint8_t configured = 0;
uint32_t logStarted = 0;

#define READING_CLEAR_TIMEOUT	3

extern USBD_HandleTypeDef hUsbDeviceFS;

volatile uint8_t m_take_reading = 0;
volatile uint8_t m_clear_screen = 0;
volatile int_button_t button;

surveyTag_config_t config;

void set_wakeup_time(uint32_t alarm_def, uint32_t seconds)
{
	RTC_TimeTypeDef currentTime;
	RTC_DateTypeDef currentDate;
	RTC_AlarmTypeDef alarm;

	// set an alarm
	alarm.Alarm = alarm_def;
	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
	alarm.AlarmTime = currentTime;
	alarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;


	// calculate the time and date of the next alarm
	addSeconds(&currentDate, &alarm.AlarmTime, seconds);
	alarm.AlarmDateWeekDay = currentDate.Date;

	alarm.AlarmMask =  RTC_ALARMMASK_NONE;

	alarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	alarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
	alarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;

	HAL_RTC_SetAlarm_IT(&hrtc, &alarm, RTC_FORMAT_BIN);

}


uint8_t alarmExpired(void){


	RTC_TimeTypeDef currentTime;
	RTC_DateTypeDef currentDate;
	RTC_AlarmTypeDef alarm;

	RTC_TimeTypeDef alarmTime;
	uint8_t alarmDate;

	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);

	HAL_RTC_GetAlarm(&hrtc, &alarm, RTC_ALARM_A, RTC_FORMAT_BIN);
	alarmTime = alarm.AlarmTime;
	alarmDate = alarm.AlarmDateWeekDay;

	// if the current date is less than the alarm date, return immediately

	if ((currentDate.Date < alarmDate) && (currentDate.Date != 1)) return 0;
	if (currentTime.Hours < alarmTime.Hours) return 0;
	if (currentTime.Minutes < alarmTime.Minutes) return 0;
	if (currentTime.Seconds < alarmTime.Seconds) return 0;

	return 1;
}

static void display_wait_screen(void)
{
	if ( rtc_set_flag(0) )
	{
		if(  logStarted  )
		{
			updateDisplay('L', 'o', 'g',  readBatteryLevel(), 0, 0, 0, 0, false, 0);
		}
		else
		{
			updateDisplay('r', 'd', 'y',  readBatteryLevel(), 0, 0, 0, 0, false, 0);
		}
	}
	else
	{
		updateDisplay('-', '-', '-',  readBatteryLevel(), 0, 0, 0, 0, false, 0);
	}
}

surveyTag_Result surveyTag_init(void)
{
	//HAL_SD_DeInit(&hsd);

	HAL_GPIO_WritePin(THERM_PWR_EN_GPIO_Port, THERM_PWR_EN_Pin, GPIO_PIN_SET); //Disable thermocouple
	HAL_GPIO_WritePin(BATT_MEAS_GPIO_Port, BATT_MEAS_Pin, GPIO_PIN_SET); //Disable battery measurement
	//HAL_GPIO_WritePin(USB_PU_GPIO_Port, USB_PU_Pin, GPIO_PIN_SET); //Enable USB pullup

	//HAL_GPIO_WritePin(SD_PWR_EN_GPIO_Port, SD_PWR_EN_Pin, GPIO_PIN_SET); // turn off SD Card

	initDisplay();

	updateDisplay('-', '-', '-',  readBatteryLevel(), 0, 0, 0, 0, false, 0);

	if(read_config_sd(&config) != st_OK)
	{
		store_default_config();
		read_config_sd(&config);
	}

	return st_OK;
}

surveyTag_Result surveyTag_iterate(void)
{
	if( HAL_GPIO_ReadPin(USB_DETECT_GPIO_Port, USB_DETECT_Pin) == GPIO_PIN_RESET ) enter_sleep();
	else usb_status(USB_OPENED);

	// check if wakeup was alarm
	if ( m_take_reading )
	{
		m_take_reading = 0;
		if ( logStarted )
		{
			surveyTag_reading_t *reading;
			set_wakeup_time(RTC_ALARM_A, config.logInterval);
			HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);
			start_temp_sampling(&config);
			while( currently_temp_sampling() );

			end_temp_sampling(&reading);
			store_reading_sd(reading);
			if( config.logInterval > READING_CLEAR_TIMEOUT )
				set_wakeup_time(RTC_ALARM_B, READING_CLEAR_TIMEOUT);
		}
	}

	// handle button pressed
	if ( button.pressed )
	{
		button.pressed = 0;
		if (button.hold_ticks > 5)
		{
			if( config.startMode == startMode_Immediate || !rtc_set_flag(0) || logStarted ) //Show a reading on button press when it will be automatically logging
			{
				HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);
				start_temp_sampling(&config);
				while( currently_temp_sampling() );
				end_temp_sampling(NULL);
				set_wakeup_time(RTC_ALARM_B, READING_CLEAR_TIMEOUT);
			}
			else if( config.startMode == startMode_ButtonPress && logStarted == 0)//In button mode, we put it into logging mode
			{
				logStarted = 1;
				updateDisplay('L', 'o', 'g',  readBatteryLevel(), 0, 0, 0, 0, false, 0);
				set_wakeup_time(RTC_ALARM_A, config.logInterval);
			}
		}
	}

	if ( m_clear_screen )
	{
		m_clear_screen = 0;
		display_wait_screen();
		HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);


	}


	// handle USB connection
	if ( usb_connected() )
	{

		MX_USB_DEVICE_Init();
		// we have usb connected
		HAL_GPIO_WritePin(USB_PU_GPIO_Port, USB_PU_Pin, GPIO_PIN_SET);

		updateDisplay('U', '5', 'b',  readBatteryLevel(), 0, 0, 0, 0, false, 0);


		while (usb_status(USB_GET) != USB_DISCONNECTED)
		{
			print_config_options();
		}

		USBD_Stop(&hUsbDeviceFS);
		USBD_DeInit(&hUsbDeviceFS);

		// set USB pins to analog?


		//HAL_GPIO_WritePin(USB_PU_GPIO_Port, USB_PU_Pin, GPIO_PIN_RESET);

		if ( rtc_set_flag(0) )
		{
			if( config.startMode == startMode_Immediate )
			{
				set_wakeup_time(RTC_ALARM_A, config.logInterval);
				logStarted = 1;
			}
			else
			{
				logStarted = 0;
			}
		}
		display_wait_screen();
	}

	return st_OK;
}


uint32_t readBatteryLevel(void)
{
	#define VREFINT_CAL_ADDR                0x1FF800F8
	#define VREFINT_CAL ((uint16_t*) VREFINT_CAL_ADDR)

	uint32_t rawValue = 0;
	uint32_t vrefValue = 0;

	HAL_ADC_Init(&hadc);
	ADC_ChannelConfTypeDef sConfig = {0};

	// turn on the battery voltage FET
	HAL_GPIO_WritePin(BATT_MEAS_GPIO_Port, BATT_MEAS_Pin, RESET);

	// sample reference so vbatt can settle
	sConfig.Channel = ADC_CHANNEL_VREFINT;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_192CYCLES;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);
	HAL_ADC_Start(&hadc);
	HAL_ADC_PollForConversion(&hadc, 100);
	vrefValue = HAL_ADC_GetValue(&hadc);

	sConfig.Channel = ADC_CHANNEL_4;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_192CYCLES;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);

	HAL_ADC_Start(&hadc);
	HAL_ADC_PollForConversion(&hadc, 100);
	rawValue = HAL_ADC_GetValue(&hadc);
	HAL_ADC_DeInit(&hadc);
	HAL_GPIO_WritePin(BATT_MEAS_GPIO_Port, BATT_MEAS_Pin, SET);

	// check against internal reference and calibration
	float battV = 3 * (float)*VREFINT_CAL * (float)rawValue / ((float)vrefValue * 4095);

	uint32_t battMV_mult = battV * 1000;
	return battMV_mult;
}


void addSeconds(RTC_DateTypeDef *date, RTC_TimeTypeDef *time, uint32_t seconds) {
    uint8_t n;
    if (seconds >= 86400) {
        n = seconds / 86400;
        date->Date += n;
        seconds -= n * 86400;
    }
    if (seconds >= 3600) {
        n = seconds / 3600;
        time->Hours += n;
        seconds -= n * 3600;
    }
    if (seconds >= 60) {
        n = seconds / 60;
        time->Minutes += n;
        seconds -= n * 60;
    }
    time->Seconds += seconds;
    if (time->Seconds >= 60) {
        time->Seconds -= 60;
        time->Minutes += 1;
    }
    if (time->Minutes >= 60) {
        time->Minutes -= 60;
        time->Hours += 1;
    }
    if (time->Hours >= 24) {
        time->Hours -= 24;
        date->Date += 1;
    }
    n = 31;
    if (date->Month == 2) {
        if (date->Year % 4 == 0 && (date->Year % 100 != 0 || date->Year % 400 == 0)) {
            n = 29;
        } else {
            n = 28;
        }
    }
    else if (date->Month == 4 || date->Month == 6 || date->Month == 9 || date->Month == 11)
    {
        n = 30;
    }
    if (date->Date > n) {
        date->Date -= n;
        date->Month += 1;
    }
    if (date->Month > 12) {
        date->Month -= 12;
        date->Year += 1;
    }
}


usb_status_t usb_status(usb_status_t status)
{
	static usb_status_t current_status = USB_DISCONNECTED;
	if(status != USB_GET)
	{
		current_status = status;
	}
	return current_status;
}


surveyTag_config_t* get_config(void)
{
	return &config;
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == USB_DETECT_Pin)
	{
		//USB detect
		if( HAL_GPIO_ReadPin(USB_DETECT_GPIO_Port, USB_DETECT_Pin) )
		{
			usb_status(USB_OPENED);
		}
		else
		{
			usb_status(USB_DISCONNECTED);
		}
	}
	else if( GPIO_Pin == TEMP_DRDY_Pin && currently_temp_sampling() )
	{
		temp_sample_cb(&config);
	}
	else if (GPIO_Pin == BUTTON_Pin)
	{
		HAL_ResumeTick();
		if( usb_connected() )
			return;

		button.pressed = 0;
		button.hold_ticks = HAL_GetTick();
		while( HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_RESET );
		button.hold_ticks = HAL_GetTick() - button.hold_ticks;
		button.pressed = 1;
	}
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
	m_take_reading = 1;
}

void HAL_RTCEx_AlarmBEventCallback(RTC_HandleTypeDef *hrtc)
{
	m_clear_screen = 1;
}

