/*
 * flash.h
 *
 *  Created on: Jul 29, 2021
 *      Author: Dave
 */

#ifndef INC_FLASH_H_
#define INC_FLASH_H_

#define FLASH_SZE			16*1024*1024
#define FLASH_PAGE_SZE		256
#define FLASH_BLOCK_SZE		4096
#define FLASH_BLOCK_CNT		FLASH_SZE / FLASH_BLOCK_SZE

void spi_flash_init(void);
void spi_flash_deinit(void);

#endif /* INC_FLASH_H_ */
