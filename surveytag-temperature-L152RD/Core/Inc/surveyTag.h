/*
//
//  _____ ___  ___ ___ ___ _____ _____
// |_   _/ _ \| _ \ _ ) __|_   _|_   _|
//   | || (_) |   / _ \ _|  | |   | |
//  _|_| \___/|_|_\___/___| |_|   |_|
// |   \ ___ __(_)__ _ _ _
// | |) / -_|_-< / _` | ' \
// |___/\___/__/_\__, |_||_|
//               |___/
//
// SurveyTag
// (c) Torbett Design Ltd 2021
 *
 * surveyTag.h
 * Main SurveyTag header and definitions
//
*/

#ifndef _TAG_h
#define _TAG_h

#include "main.h"
#include "adc.h"
//#include "fatfs.h"
#include "lcd.h"
#include "rtc.h"
//#include "sdio.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
#include "usbd_core.h"
#include "stdbool.h"
#include "string.h"
#include "../../Middlewares/Third_Party/MAXIM/MAX31856drv.h"

#define SURVEYTAG_VERSION	"v1.1"

typedef enum
{
	st_OK = 0,
	st_ERROR
}
surveyTag_Result;

typedef enum
{
	tc_typeB,
	tc_typeE,
	tc_typeJ,
	tc_typeK,
	tc_typeN,
	tc_typeR,
	tc_typeS,
	tc_typeT
}
thermocoupleType_t;


typedef enum
{
	startMode_Immediate,
	startMode_ButtonPress,
	startMode_TimeDelay
}
start_mode_t;

typedef enum
{
	USB_GET,
	USB_DISCONNECTED,
	USB_WAIT_OPEN,
	USB_OPENED,
}
usb_status_t;

typedef struct
{
	char loggerName[30];
	uint32_t logInterval;
	uint32_t burstLength; //mS
	uint8_t averageSamples;
	thermocoupleType_t tc_type;
	uint8_t contrast;
	uint32_t samplesPerFile;
	int16_t cal_offset;
	start_mode_t startMode;
	uint8_t reserved[128]; //Reserved for future use
}
surveyTag_config_t;


typedef struct
{
	float tc_reading; 			// in millidegrees (C), i.e. 1000 = 1 degree
	float cold_junction_reading;  // same as above
	uint16_t batteryLevel;			// millivolts, 1000 = 1v
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint16_t num_readings;
	uint8_t fault_code;
	uint8_t average_samples;
}
surveyTag_reading_t;

typedef struct
{
	uint8_t pressed;
	uint32_t hold_ticks;
}
int_button_t;


//extern uint8_t retSD; /* Return value for SD */
//extern char SDPath[4]; /* SD logical drive path */
//extern FATFS SDFatFS; /* File system object for SD logical drive */
//extern FIL SDFile; /* File object for SD */

surveyTag_Result mountSDCard(void);
surveyTag_Result initLCD(void);
uint32_t readBatteryLevel(void);
void SetPinsAnalog(void);
void GoToSleep(void);
surveyTag_Result ReadConfigFile();
void Clock_Config(void);
surveyTag_Result startLogFile(void);
surveyTag_Result takeReading(surveyTag_reading_t* reading);
surveyTag_Result writeRecord(surveyTag_reading_t* record);
surveyTag_Result readTimeDateFile(void);
void addSeconds(RTC_DateTypeDef *date, RTC_TimeTypeDef *time, uint32_t seconds);
surveyTag_config_t* get_config(void);


usb_status_t usb_status(usb_status_t status);
#define usb_connected() (usb_status(USB_GET) == USB_OPENED)
#endif
