/*
 * lcd_user.h
 *
 *  Created on: 26 Jul 2021
 *      Author: Dave
 */

#ifndef INC_LCD_USER_H_
#define INC_LCD_USER_H_

#include <surveyTag.h>

#define LCD_1A HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b10, 0b10)
#define LCD_1B HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b10, 0b10)
#define LCD_1C HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b10, 0b10)
#define LCD_1D HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b10, 0b10)
#define LCD_1E HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b01, 0b01)
#define LCD_1F HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b01, 0b01)
#define LCD_1G HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b01, 0b01)

#define LCD_2A HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b1000,0b1000)
#define LCD_2B HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b1000,0b1000)
#define LCD_2C HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b1000,0b1000)
#define LCD_2D HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b1000,0b1000)
#define LCD_2E HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b0100,0b0100)
#define LCD_2F HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b0100,0b0100)
#define LCD_2G HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b0100,0b0100)

#define LCD_3A HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b100000,0b100000)
#define LCD_3B HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b100000,0b100000)
#define LCD_3C HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b100000,0b100000)
#define LCD_3D HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER0, ~0b100000,0b100000)
#define LCD_3E HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER2, ~0b010000,0b010000)
#define LCD_3F HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER6, ~0b010000,0b010000)
#define LCD_3G HAL_LCD_Write(&hlcd, LCD_RAM_REGISTER4, ~0b010000,0b010000)

surveyTag_Result initDisplay(void);
surveyTag_Result updateDisplay(uint8_t char1, uint8_t char2, uint8_t char3, uint16_t batteryLevel, bool leadingOne, bool leadingNegative, bool degC, bool decPoint, bool percent, uint8_t signal_level);

#endif /* INC_LCD_USER_H_ */
