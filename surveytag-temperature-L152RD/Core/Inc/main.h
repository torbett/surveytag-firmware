/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BUTTON_Pin GPIO_PIN_13
#define BUTTON_GPIO_Port GPIOC
#define BUTTON_EXTI_IRQn EXTI15_10_IRQn
#define FLASH_PWR_EN_Pin GPIO_PIN_0
#define FLASH_PWR_EN_GPIO_Port GPIOC
#define THERM_PWR_EN_Pin GPIO_PIN_1
#define THERM_PWR_EN_GPIO_Port GPIOC
#define THERM_FAULT_Pin GPIO_PIN_2
#define THERM_FAULT_GPIO_Port GPIOC
#define USB_PU_Pin GPIO_PIN_3
#define USB_PU_GPIO_Port GPIOC
#define VBATT_ADC_Pin GPIO_PIN_4
#define VBATT_ADC_GPIO_Port GPIOA
#define BATT_MEAS_Pin GPIO_PIN_5
#define BATT_MEAS_GPIO_Port GPIOA
#define DEBUGIO1_Pin GPIO_PIN_4
#define DEBUGIO1_GPIO_Port GPIOC
#define DEBUGIO2_Pin GPIO_PIN_5
#define DEBUGIO2_GPIO_Port GPIOC
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define TEMP_DRDY_Pin GPIO_PIN_6
#define TEMP_DRDY_GPIO_Port GPIOC
#define TEMP_DRDY_EXTI_IRQn EXTI9_5_IRQn
#define USB_DETECT_Pin GPIO_PIN_7
#define USB_DETECT_GPIO_Port GPIOC
#define USB_DETECT_EXTI_IRQn EXTI9_5_IRQn
#define FLASH_CS_Pin GPIO_PIN_9
#define FLASH_CS_GPIO_Port GPIOC
#define SDIO_DETECT_Pin GPIO_PIN_15
#define SDIO_DETECT_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
