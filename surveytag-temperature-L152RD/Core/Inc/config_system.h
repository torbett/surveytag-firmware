/*
 * config_system.h
 *
 *  Created on: Jul 28, 2021
 *      Author: Dave
 */

#ifndef INC_CONFIG_SYSTEM_H_
#define INC_CONFIG_SYSTEM_H_

uint8_t rtc_set_flag(int8_t flag);

void print_config_options(void);

#endif /* INC_CONFIG_SYSTEM_H_ */
