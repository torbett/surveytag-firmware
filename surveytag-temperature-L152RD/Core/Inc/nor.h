/*
 * sd.h
 *
 *  Created on: 26 Jul 2021
 *      Author: Dave
 */

#ifndef INC_NOR_H_
#define INC_NOR_H_

#include <surveyTag.h>

#define CONFIG_FILE_PATH	"config.bin"
#define DATA_FILE_PATH		"data.bin"

surveyTag_Result mount_sd(void);
surveyTag_Result unmount_sd(void);
surveyTag_Result read_config_sd(surveyTag_config_t *config);
surveyTag_Result write_config_sd(const surveyTag_config_t *config);
surveyTag_Result store_reading_sd(surveyTag_reading_t *reading);
void store_default_config();

#endif
