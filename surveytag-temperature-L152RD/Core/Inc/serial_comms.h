/*
 * serial_comms.h
 *
 *  Created on: 26 Jul 2021
 *      Author: Dave
 */

#ifndef INC_SERIAL_COMMS_H_
#define INC_SERIAL_COMMS_H_

#define log_detail(...)       serial_output("[verb.]  " __VA_ARGS__)
#define log_info(...)         serial_output("[info ]    " __VA_ARGS__)
#define log_warning(...)      serial_output("[warn ] " __VA_ARGS__)
#define log_error(...)        serial_output("[error]   " __VA_ARGS__)

#include <stdint.h>

void serial_output(const char* format, ...);
char serial_get_char(uint8_t flush_buffer);
char* serial_read_line(void);
uint8_t serial_print_menu(const char* options);

#endif /* INC_SERIAL_COMMS_H_ */
