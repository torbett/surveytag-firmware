/*
 * power.h
 *
 *  Created on: 27 Jul 2021
 *      Author: Dave
 */

#ifndef INC_POWER_H_
#define INC_POWER_H_

void set_pins_for_sleep(void);

void enter_sleep(void);

void configure_clocks(void);

#endif /* INC_POWER_H_ */
