/*
 * readings.h
 *
 *  Created on: Jul 23, 2021
 *      Author: Dave
 */

#ifndef INC_READINGS_H_
#define INC_READINGS_H_

#include "surveyTag.h"

surveyTag_Result start_temp_sampling(surveyTag_config_t *config);

surveyTag_Result temp_sample_cb(surveyTag_config_t *config);

surveyTag_Result end_temp_sampling(surveyTag_reading_t **reading);

uint8_t currently_temp_sampling(void);

#endif /* INC_READINGS_H_ */
