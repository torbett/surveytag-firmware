/*
 * lfs.h
 *
 *  Created on: Jul 29, 2021
 *      Author: Dave
 */

#ifndef INC_LFS_H_
#define INC_LFS_H_

#include "surveyTag.h"
#include "../../Middlewares/Third_Party/lfs/lfs.h"

surveyTag_Result lfs_init(void);
surveyTag_Result lfs_deinit(void);

surveyTag_Result do_format(void);
surveyTag_Result lfs_open(lfs_file_t *file, const char* path, int flags);
surveyTag_Result lfs_read(lfs_file_t *file, void* data, size_t len);
surveyTag_Result lfs_write(lfs_file_t *file, void* data, size_t len);
surveyTag_Result lfs_size(lfs_file_t *file, size_t *size);
surveyTag_Result lfs_close(lfs_file_t *file);
surveyTag_Result lfs_delete(const char* path);

#endif /* INC_LFS_H_ */
